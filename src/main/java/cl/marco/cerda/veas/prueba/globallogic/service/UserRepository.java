package cl.marco.cerda.veas.prueba.globallogic.service;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import cl.marco.cerda.veas.prueba.globallogic.model.User;

public interface UserRepository extends CrudRepository<User, Long> {
	
	public List<User> findByEmail(String email);
	
	@Query("select u from User u where u.email = :email and u.password = :password")
	public List<User> findByEmailAndPassword(@Param("email") String email,@Param("password") String password);
	

}
