package cl.marco.cerda.veas.prueba.globallogic.excpetion;

import org.springframework.http.HttpStatus;

import lombok.Getter;

public class PersistenceGlobalLogicException extends Exception {
	
	@Getter
	private final HttpStatus status;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PersistenceGlobalLogicException(String mensaje, HttpStatus status){		
		super(mensaje);
		this.status = status;
	}

}
