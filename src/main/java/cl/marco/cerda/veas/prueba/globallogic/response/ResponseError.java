package cl.marco.cerda.veas.prueba.globallogic.response;

import lombok.Data;

@Data
public class ResponseError implements GenericResponse{

	private String mensaje;
}
