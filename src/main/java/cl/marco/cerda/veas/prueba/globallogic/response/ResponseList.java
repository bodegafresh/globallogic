package cl.marco.cerda.veas.prueba.globallogic.response;

import java.util.List;

import cl.marco.cerda.veas.prueba.globallogic.dto.UserDto;
import lombok.Getter;
import lombok.Setter;

public class ResponseList implements GenericResponse {
	@Getter @Setter
	private List<UserDto> users;
}
