package cl.marco.cerda.veas.prueba.globallogic.dto;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import cl.marco.cerda.veas.prueba.globallogic.response.GenericResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDto implements GenericResponse, Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	private Long id;

	@NotNull(message = "Nombre no puede ser nulo")
	@NotBlank(message = "Nombre no puede ser vacio")
	private String name;

	@NotNull(message = "Password no puede ser nulo")
	@NotBlank(message = "Password no puede ser vacio")
	@Pattern(regexp = "^(?=\\w*\\d)(?=\\w*[A-Z])(?=\\w*[a-z])\\S{4,}$", message = "Password debe contener una mayuscula y dos digitos")
	private String password;

	@NotNull(message = "Email no puede ser nulo")
	@NotBlank(message = "Email no puede ser vacio")
	@Pattern(regexp = "^(.+)@(.+)$", message = "Email debe ser valido")
	private String email;

	@NotEmpty(message = "Phones no pude ser vacio")
	@OneToMany(targetEntity = PhoneDto.class, mappedBy = "id", fetch = FetchType.EAGER)
	private List<PhoneDto> phones;

	private LocalDateTime created;

	private LocalDateTime modified;

	private LocalDateTime lastLogin;

	private boolean isActive;

	@JsonInclude(value = Include.NON_NULL)
	private String token;
	

}
