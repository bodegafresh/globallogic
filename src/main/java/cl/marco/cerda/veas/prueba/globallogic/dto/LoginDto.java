package cl.marco.cerda.veas.prueba.globallogic.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class LoginDto {

	@NotNull(message = "Email no puede ser nulo")
	@NotBlank(message = "Email no puede ser vacio")
	@Pattern(regexp = "^(.+)@(.+)$", message = "Email debe ser valido")
	private String email;
	
	private String password;
}
