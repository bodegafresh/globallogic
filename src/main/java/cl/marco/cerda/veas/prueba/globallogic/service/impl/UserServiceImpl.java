package cl.marco.cerda.veas.prueba.globallogic.service.impl;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import cl.marco.cerda.veas.prueba.globallogic.dto.PhoneDto;
import cl.marco.cerda.veas.prueba.globallogic.dto.UserDto;
import cl.marco.cerda.veas.prueba.globallogic.excpetion.PersistenceGlobalLogicException;
import cl.marco.cerda.veas.prueba.globallogic.model.Phone;
import cl.marco.cerda.veas.prueba.globallogic.model.User;
import cl.marco.cerda.veas.prueba.globallogic.service.UserRepository;
import cl.marco.cerda.veas.prueba.globallogic.service.UserService;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class UserServiceImpl implements UserService {
	private static final String EMAIL_EXIST = "El correo ya registrado";
	private static final String USER_ID_NOT_FOUND = "Usuario no encontrado";
	private static final String CONCAT_ERROR_MENSAJE = "id: {}";

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private PasswordEncoder bcryptEncoder;

	@Override
	public UserDto createUser(UserDto requestUser) throws PersistenceGlobalLogicException {
		log.info("Creando usuario {}", requestUser.getName());
		List<User> usersByEmail = userRepository.findByEmail(requestUser.getEmail());
		if (!usersByEmail.isEmpty()) {
			log.info(EMAIL_EXIST + ": {}", requestUser.getEmail());
			throw new PersistenceGlobalLogicException(EMAIL_EXIST, HttpStatus.CONFLICT);
		}		
		String pass = bcryptEncoder.encode(requestUser.getPassword());		
		User user = User.builder()
				.created(LocalDateTime.now())
				.email(requestUser.getEmail())
				.isActive(true)
				.lastLogin(LocalDateTime.now())
				.modified(LocalDateTime.now())
				.name(requestUser.getName())
				.password(pass)
				.phones(requestUser
						.getPhones().stream().map(r -> Phone.builder()
								.citycode(r.getCitycode())
								.contrycode(r.getContrycode())
								.number(r.getNumber())
								.build())
						.collect(Collectors.toList()))
				.build();
		userRepository.save(user);
		requestUser.setActive(user.isActive());
		requestUser.setCreated(user.getCreated());
		requestUser.setId(user.getId());
		requestUser.setLastLogin(user.getLastLogin());
		requestUser.setModified(user.getModified());
		log.info("Usuario guardado");
		return requestUser;
	}

	@Override
	public UserDto getUserById(Long userId) throws PersistenceGlobalLogicException {
		Optional<User> optUser = userRepository.findById(userId);
		User user = optUser.isPresent() ? optUser.get() : null;
		if (user == null) {
			log.info(USER_ID_NOT_FOUND + CONCAT_ERROR_MENSAJE, userId);
			throw new PersistenceGlobalLogicException(USER_ID_NOT_FOUND, HttpStatus.NOT_FOUND);
		}
		return UserDto.builder()
				.created(user.getCreated())
				.email(user.getEmail())
				.id(user.getId())
				.isActive(user.isActive())
				.lastLogin(user.getLastLogin())
				.modified(user.getModified())
				.name(user.getName())
				.password(user.getPassword())
				.phones(user
						.getPhones().stream().map(p -> PhoneDto
								.builder()
								.citycode(p.getCitycode())
								.contrycode(p.getContrycode())
								.id(p.getId())
								.number(p.getNumber())
								.build())
						.collect(Collectors.toList()))
				.build();

	}

	@Override
	public UserDto updateUserById(Long userId, UserDto requestUser) throws PersistenceGlobalLogicException {
		Optional<User> optUser = userRepository.findById(userId);
		User user = optUser.isPresent() ? optUser.get() : null;
		if (user == null) {
			log.info(USER_ID_NOT_FOUND + CONCAT_ERROR_MENSAJE, userId);
			throw new PersistenceGlobalLogicException(USER_ID_NOT_FOUND, HttpStatus.NOT_FOUND);
		}
		user.setActive(requestUser.isActive());
		user.setEmail(requestUser.getEmail());
		user.setName(requestUser.getName());
		user.setPassword(requestUser.getPassword());
		user.setPhones(
				requestUser
						.getPhones().stream().map(r -> Phone.builder()
								.citycode(r.getCitycode())
								.contrycode(r.getContrycode())
								.number(r.getNumber())
								.id(r.getId())
								.build())
						.collect(Collectors.toList()));
		user.setModified(LocalDateTime.now());
		userRepository.save(user);
		requestUser.setActive(user.isActive());
		requestUser.setCreated(user.getCreated());
		requestUser.setId(user.getId());
		requestUser.setLastLogin(user.getLastLogin());
		requestUser.setModified(user.getModified());
		return requestUser;
	}

	@Override
	public UserDto deleteUserById(Long userId) throws PersistenceGlobalLogicException {
		Optional<User> optUser = userRepository.findById(userId);
		User user = optUser.isPresent() ? optUser.get() : null;
		if (user == null) {
			log.info(USER_ID_NOT_FOUND + CONCAT_ERROR_MENSAJE, userId);
			throw new PersistenceGlobalLogicException(USER_ID_NOT_FOUND, HttpStatus.NOT_FOUND);
		}
		UserDto userDto = UserDto.builder()
				.created(user.getCreated())
				.email(user.getEmail())
				.id(user.getId())
				.isActive(user.isActive())
				.lastLogin(user.getLastLogin())
				.modified(user.getModified())
				.name(user.getName()).password(user.getPassword())
				.phones(user
						.getPhones().stream().map(p -> PhoneDto.builder()
								.citycode(p.getCitycode())
								.contrycode(p.getContrycode())
								.id(p.getId())
								.number(p.getNumber())
								.build())
						.collect(Collectors.toList()))
				.build();
		userRepository.delete(user);
		return userDto;
	}

	@Override
	public List<UserDto> getAllUser() throws PersistenceGlobalLogicException {
		List<User> users = (List<User>) userRepository.findAll();

		return users.stream()
				.map(user -> UserDto.builder()
						.created(user.getCreated())
						.email(user.getEmail())
						.id(user.getId())
						.isActive(user.isActive())
						.lastLogin(user.getLastLogin())
						.modified(user.getModified())
						.name(user.getName())
						.password(user.getPassword())
						.phones(user.getPhones().stream()
								.map(p -> PhoneDto.builder()
										.citycode(p.getCitycode())
										.contrycode(p.getContrycode())
										.id(p.getId())
										.number(p.getNumber())
										.build())
								.collect(Collectors.toList()))
						.build())
				.collect(Collectors.toList());
	}

	@Override
	public UserDto login(String email, String password) throws PersistenceGlobalLogicException {		
		UserDetails user = this.loadUserByUsername(email);
		if (user == null) {
			throw new PersistenceGlobalLogicException(USER_ID_NOT_FOUND, HttpStatus.NOT_FOUND);
		}
		if (!bcryptEncoder.matches(password, user.getPassword())) {
			throw new PersistenceGlobalLogicException("INVALID_CREDENTIALS", HttpStatus.NOT_FOUND);
		}
		
		if (!((User) user).isActive()) {
			throw new PersistenceGlobalLogicException("User no active", HttpStatus.CONFLICT);
		}
		return UserDto.builder()
				.created(((User) user).getCreated())
				.email(((User) user).getEmail())
				.id(((User) user).getId())
				.isActive(((User) user).isActive())
				.lastLogin(((User) user).getLastLogin())
				.modified(((User) user).getModified())
				.name(user.getUsername())
				.password(user.getPassword())
				.phones(((User) user)
						.getPhones().stream().map(p -> PhoneDto
								.builder()
								.citycode(p.getCitycode())
								.contrycode(p.getContrycode())
								.id(p.getId())
								.number(p.getNumber())
								.build())
						.collect(Collectors.toList()))
				.build();
	}

	@Override
	public UserDto getUserByEmail(String email) throws PersistenceGlobalLogicException {
		List<User> optUser = userRepository.findByEmail(email);
		User user = optUser.size()==1 ? optUser.get(0) : null;
		if (user == null) {
			log.info(USER_ID_NOT_FOUND + CONCAT_ERROR_MENSAJE, email);
			throw new PersistenceGlobalLogicException(USER_ID_NOT_FOUND, HttpStatus.NOT_FOUND);
		}
		return UserDto.builder()
				.created(user.getCreated())
				.email(user.getEmail())
				.id(user.getId())
				.isActive(user.isActive())
				.lastLogin(user.getLastLogin())
				.modified(user.getModified())
				.name(user.getName())
				.password(user.getPassword())
				.phones(user
						.getPhones().stream().map(p -> PhoneDto
								.builder()
								.citycode(p.getCitycode())
								.contrycode(p.getContrycode())
								.id(p.getId())
								.number(p.getNumber())
								.build())
						.collect(Collectors.toList()))
				.build();
	}

	@Override
	public UserDetails loadUserByUsername(String username)  {
		List<User> userList = userRepository.findByEmail(username);
        if (userList == null || userList.isEmpty()) {
            throw new UsernameNotFoundException(username);
        }
        return userList.get(0);
         
	}

}
