package cl.marco.cerda.veas.prueba.globallogic.service;

import java.util.List;

import org.springframework.security.core.userdetails.UserDetailsService;

import cl.marco.cerda.veas.prueba.globallogic.dto.UserDto;
import cl.marco.cerda.veas.prueba.globallogic.excpetion.PersistenceGlobalLogicException;

public interface UserService extends UserDetailsService{
	
	public UserDto createUser(UserDto user) throws PersistenceGlobalLogicException ;

	public UserDto getUserById(Long userId) throws PersistenceGlobalLogicException;
	
	public UserDto getUserByEmail(String email) throws PersistenceGlobalLogicException;
	
	public UserDto updateUserById(Long userId, UserDto requestUser) throws PersistenceGlobalLogicException;
	
	public UserDto deleteUserById(Long userId) throws PersistenceGlobalLogicException;
	
	public List<UserDto> getAllUser() throws PersistenceGlobalLogicException;
	
	public UserDto login(String email, String password) throws PersistenceGlobalLogicException;

}
