package cl.marco.cerda.veas.prueba.globallogic.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import cl.marco.cerda.veas.prueba.globallogic.utils.JwtTokenUtil;
import cl.marco.cerda.veas.prueba.globallogic.dto.LoginDto;
import cl.marco.cerda.veas.prueba.globallogic.dto.UserDto;
import cl.marco.cerda.veas.prueba.globallogic.excpetion.AuthorizationGlobalLogicException;
import cl.marco.cerda.veas.prueba.globallogic.excpetion.PersistenceGlobalLogicException;
import cl.marco.cerda.veas.prueba.globallogic.response.GenericResponse;
import cl.marco.cerda.veas.prueba.globallogic.response.ResponseError;
import cl.marco.cerda.veas.prueba.globallogic.response.ResponseList;
import cl.marco.cerda.veas.prueba.globallogic.service.UserService;
import lombok.extern.log4j.Log4j2;

@RestController
@Log4j2
public class UserController {

	private static final String PERSISTENCE_ERROR = "PersistenceException";
	private static final String UNKNOWN_ERROR = "Error desconocido";

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private UserService userService;

	@PostMapping(value = "/create", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<GenericResponse> registryUser(@Valid @RequestBody UserDto user) {
		ResponseEntity<GenericResponse> response = null;
		log.info("Creando nuevo usuario");
		try {
			userService.createUser(user);
			user.setPassword(null);
			response = new ResponseEntity<>(user, HttpStatus.OK);
		} catch (PersistenceGlobalLogicException e) {
			ResponseError error = new ResponseError();
			log.error(PERSISTENCE_ERROR, e);
			error.setMensaje(e.getMessage());
			response = new ResponseEntity<>(error, e.getStatus());
		} catch (Exception e) {
			log.error(UNKNOWN_ERROR, e);
			ResponseError error = new ResponseError();
			error.setMensaje(e.getMessage());
			response = new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return response;
	}

	@GetMapping(value = "/user/{userId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<GenericResponse> getUser(@PathVariable("userId") Long userId) {
		ResponseEntity<GenericResponse> response = null;
		log.info("Buscando usuario por Id");
		try {
			UserDto user = userService.getUserById(userId);
			response = new ResponseEntity<>(user, HttpStatus.OK);
		} catch (PersistenceGlobalLogicException e) {
			ResponseError error = new ResponseError();
			log.error(PERSISTENCE_ERROR, e);
			error.setMensaje(e.getMessage());
			response = new ResponseEntity<>(error, e.getStatus());
		} catch (Exception e) {
			log.error(UNKNOWN_ERROR, e);
			ResponseError error = new ResponseError();
			error.setMensaje(e.getMessage());
			response = new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return response;
	}

	@PutMapping(value = "/user/{userId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<GenericResponse> updateUser(@PathVariable("userId") Long userId,
			@Valid @RequestBody UserDto userDto) {
		ResponseEntity<GenericResponse> response = null;
		log.info("Actualizando usuario por Id");
		try {
			UserDto user = userService.updateUserById(userId, userDto);
			response = new ResponseEntity<>(user, HttpStatus.OK);
		} catch (PersistenceGlobalLogicException e) {
			ResponseError error = new ResponseError();
			log.error(PERSISTENCE_ERROR, e);
			error.setMensaje(e.getMessage());
			response = new ResponseEntity<>(error, e.getStatus());
		} catch (Exception e) {
			log.error(UNKNOWN_ERROR, e);
			ResponseError error = new ResponseError();
			error.setMensaje(e.getMessage());
			response = new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return response;
	}

	@DeleteMapping(value = "/user/{userId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<GenericResponse> deleteUser(@PathVariable("userId") Long userId) {
		ResponseEntity<GenericResponse> response = null;
		log.info("Eliminando usuario por Id");
		try {
			UserDto user = userService.deleteUserById(userId);
			user.setPassword(null);
			response = new ResponseEntity<>(user, HttpStatus.OK);
		} catch (PersistenceGlobalLogicException e) {
			ResponseError error = new ResponseError();
			log.error(PERSISTENCE_ERROR, e);
			error.setMensaje(e.getMessage());
			response = new ResponseEntity<>(error, e.getStatus());
		} catch (Exception e) {
			log.error(UNKNOWN_ERROR, e);
			ResponseError error = new ResponseError();
			error.setMensaje(e.getMessage());
			response = new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return response;
	}

	@GetMapping(value = "/user", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<GenericResponse> getAllUser() {
		ResponseEntity<GenericResponse> response = null;
		log.info("Obtiene todos los usuarios");
		try {
			List<UserDto> users = userService.getAllUser();
			ResponseList responseList = new ResponseList();
			responseList.setUsers(users);
			response = new ResponseEntity<>(responseList, HttpStatus.OK);
		} catch (PersistenceGlobalLogicException e) {
			ResponseError error = new ResponseError();
			log.error(PERSISTENCE_ERROR, e);
			error.setMensaje(e.getMessage());
			response = new ResponseEntity<>(error, e.getStatus());
		} catch (Exception e) {
			log.error(UNKNOWN_ERROR, e);
			ResponseError error = new ResponseError();
			error.setMensaje(e.getMessage());
			response = new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return response;
	}

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseError handleValidationExceptions(MethodArgumentNotValidException ex) {
		ResponseError errorSalida = new ResponseError();

		List<String> errors = new ArrayList<>();
		ex.getBindingResult().getAllErrors().forEach(error -> {
			String errorMessage = error.getDefaultMessage();
			errors.add(errorMessage);
		});

		ObjectMapper mapJson = new ObjectMapper();

		try {
			String json = mapJson.writeValueAsString(errors);
			errorSalida.setMensaje(json);
		} catch (JsonProcessingException e) {
			log.error("Error escribiendo lista ", e);
		}
		return errorSalida;
	}

	@PostMapping(value = "/authenticate", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<GenericResponse> createAuthenticationToken(@RequestBody LoginDto loginDto) {
		ResponseEntity<GenericResponse> response = null;
		log.info("authenticate");
		try {
			authenticate(loginDto.getEmail(), loginDto.getPassword());

			final UserDetails user = userService.loadUserByUsername(loginDto.getEmail());
			final String token = jwtTokenUtil.generateToken(user);
			UserDto userResponse = userService.getUserByEmail(user.getUsername());
			userResponse.setPassword(null);
			userResponse.setToken(token);
			response = new ResponseEntity<>(userResponse, HttpStatus.OK);
		} catch (AuthorizationGlobalLogicException e) {
			ResponseError error = new ResponseError();
			log.error("AuthorizationGlobalLogicException", e);
			error.setMensaje(e.getMessage());
			response = new ResponseEntity<>(error, e.getStatus());
		} catch (Exception e) {
			log.error(UNKNOWN_ERROR, e);
			ResponseError error = new ResponseError();
			error.setMensaje(e.getMessage());
			response = new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return response;
	}

	private void authenticate(String username, String password) throws AuthorizationGlobalLogicException {
		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
		} catch (DisabledException e) {
			throw new AuthorizationGlobalLogicException("USER_DISABLED", HttpStatus.UNAUTHORIZED);
		} catch (BadCredentialsException e) {
			throw new AuthorizationGlobalLogicException("INVALID_CREDENTIALS", HttpStatus.UNAUTHORIZED);
		}
	}

}
