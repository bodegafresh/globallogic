package cl.marco.cerda.veas.prueba.globallogic.controller;


import spock.lang.Specification

public class UserControllerTest extends Specification {
	void 'invertir una cadena de texto'() {

		given: 'una cadena de text'
		def miCadena = 'Hola Genbetadev'

		when: 'la invertimos'
		def cadenaInvertida = miCadena.reverse()

		then: 'se invierte correctamente'
		cadenaInvertida == 'vedatebneG aloH'
	}
}
