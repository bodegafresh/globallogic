package cl.marco.cerda.veas.prueba.globallogic.controller;


import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import cl.marco.cerda.veas.prueba.globallogic.dto.UserDto;
import cl.marco.cerda.veas.prueba.globallogic.excpetion.PersistenceGlobalLogicException;
import cl.marco.cerda.veas.prueba.globallogic.model.User;
import cl.marco.cerda.veas.prueba.globallogic.service.UserRepository;
import cl.marco.cerda.veas.prueba.globallogic.service.UserService;


@RunWith(MockitoJUnitRunner.class)
class UserControllerTest {
			
	private UserRepository userRepository = Mockito.mock(UserRepository.class);
	private UserService userService = Mockito.mock(UserService.class);
	
	@Autowired
    ApplicationContext context;
	
	@BeforeEach
	public void init() {
		userRepository = Mockito.mock(UserRepository.class);
		userService = Mockito.mock(UserService.class);	
	}
	

	@Test
	void testGetUser() throws PersistenceGlobalLogicException {
		User user = User.builder().name("name").build();
		UserDto dtoUser
		when(userRepository.save(user)).thenReturn(User.builder().id(5L).build());
		User test = userRepository.save(user);		
		System.out.println(test);
		assertEquals(Long.valueOf(5L), test.getId());
		
	}

	@Test
	void testUpdateUser() {
		User user = User.builder().name("name").build();
		when(userRepository.save(user)).thenReturn(User.builder().id(5L).build());
		User test = userRepository.save(user);		
		System.out.println(test);
		assertEquals(Long.valueOf(5L), test.getId());
	}

	@Test
	void testDeleteUser() {
		User user = User.builder().name("name").build();
		when(userRepository.save(user)).thenReturn(User.builder().id(5L).build());
		User test = userRepository.save(user);		
		System.out.println(test);
		assertEquals(Long.valueOf(5L), test.getId());
	}

	@Test
	void testGetAllUser() {
		User user = User.builder().name("name").build();
		when(userRepository.save(user)).thenReturn(User.builder().id(5L).build());
		User test = userRepository.save(user);		
		System.out.println(test);
		assertEquals(Long.valueOf(5L), test.getId());
	}

	@Test
	void testCreateAuthenticationToken() {
		User user = User.builder().name("name").build();
		when(userRepository.save(user)).thenReturn(User.builder().id(5L).build());
		User test = userRepository.save(user);		
		System.out.println(test);
		assertEquals(Long.valueOf(5L), test.getId());
	}

}
