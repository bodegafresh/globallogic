# GlobalLogic

Repositorio con la respuesta para la prueba de GlobalLogic para postular al cargo en el banco BCI

- Autor: *Marco Cerda Veas*
- Email: *marco.cerda.veas@gmail.com*

### Software requerido

- java 8

### Ejecutar en local

```bash
git clone git@gitlab.com:bodegafresh/globallogic.git
cd globallogic
./gradlew bootRun

```

## Diagramas

### Diagrama de componentes

![Alt text](./documents/Componente.png?raw=true "Diagrama de components")

### Diagrama de secuencia (Crear usuario)

![Alt text](./documents/Secuencia-crea-usuario.png?raw=true "Diagrama de secuencia")



### Paths of the api:

```path
POST    /create   	--> Para registrar un usuario
POST    /authenticate	--> Para autenticar un usuario registrado
GET     /user/{userId}  --> Para obtener los datos de un usuario en base a su id
PUT     /user/{userId}  --> Para actualizar un usuario en base a su id
GET     /user		--> Para obtener todos los usuarios
DELETE  /user/{userId}	--> Para eliminar un usuario en base a su id
```

### POST /create

## headers
	- Content-Type: application/json

## request
```json
{
  "name": "Marco Cerda",
  "email": "mcerda@mcaerda2.com",
  "password": "huntEer233E",
  "phones": [
    {
      "number": "1234567",
      "citycode": "1",
      "contrycode": "57"
    }
  ]
}
```

## response
```json
{
  "id": 5,
  "name": "Marco Cerda",
  "password": null,
  "email": "mcerda@mcaedrda2.com",
  "phones": [
    {
      "id": null,
      "number": "1234567",
      "citycode": "1",
      "contrycode": "57"
    }
  ],
  "created": "2020-08-12T20:03:00.924660575",
  "modified": "2020-08-12T20:03:00.924671088",
  "lastLogin": "2020-08-12T20:03:00.924669798",
  "active": true
}
```

### POST /authenticate

## headers
	- Content-Type: application/json

## request
```json
{
	
  "email": "mcerda@mcerda2.com",
  "password": "huntEer233E"
}
```

## response
```json
{
  "id": 1,
  "name": "Marco Cerda",
  "password": null,
  "email": "mcerda@mcerda2.com",
  "phones": [
    {
      "id": 2,
      "number": "1234567",
      "citycode": "1",
      "contrycode": "57"
    }
  ],
  "created": "2020-08-12T19:37:25.625144",
  "modified": "2020-08-12T19:37:25.625158",
  "lastLogin": "2020-08-12T19:37:25.625156",
  "token": "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJtY2VyZGFAbWNlcmRhMi5jb20iLCJleHAiOjE1OTcyOTM1MTAsImlhdCI6MTU5NzI3NTUxMH0.TVbPc_vNa0rKkXzNsw35ug8z1LlrDg_uUH_0EXqQfgHpY68_VYpa5tsW4S0FVFGxfSmvwuimIITl7ZzicomC0Q",
  "active": true
}
```

### GET /user/{userId}

## headers
	- Content-Type: application/json
	- Authorization: Bearer <token>

## request
```json
no body
```

## response
```json
{
  "id": 1,
  "name": "Marco Cerda",
  "password": "$2a$10$ZFLLkrqU2clpztzakzpozu6DCQHtBGiKSIMxqZWsPtd/L97GxrApa",
  "email": "mcerda@mcerda.com",
  "phones": [
    {
      "id": 2,
      "number": "1234567",
      "citycode": "1",
      "contrycode": "57"
    }
  ],
  "created": "2020-08-12T20:07:31.818648",
  "modified": "2020-08-12T20:07:31.818667",
  "lastLogin": "2020-08-12T20:07:31.818664",
  "active": true
}
```


### PUT /user/{userId}

## headers
	- Content-Type: application/json
	- Authorization: Bearer <token>

## request
```json
{
  "name": "Marco Cerda",
  "email": "mcerda@mcaerda2.com",
  "password": "huntEer233E",
  "phones": [
    {
      "number": "1234567",
      "citycode": "1",
      "contrycode": "57"
    }
  ]
}
```

## response
```json
{
  "id": 5,
  "name": "Marco Cerda",
  "password": null,
  "email": "mcerda@mcaedrda2.com",
  "phones": [
    {
      "id": null,
      "number": "1234567",
      "citycode": "1",
      "contrycode": "57"
    }
  ],
  "created": "2020-08-12T20:03:00.924660575",
  "modified": "2020-08-12T20:03:00.924671088",
  "lastLogin": "2020-08-12T20:03:00.924669798",
  "active": true
}
```

### GET /user

## headers
	- Content-Type: application/json
	- Authorization: Bearer <token>

## request
```json
no body
```

## response
```json
{
  "users": [
    {
      "id": 1,
      "name": "Marco Cerda",
      "password": "$2a$10$ZFLLkrqU2clpztzakzpozu6DCQHtBGiKSIMxqZWsPtd/L97GxrApa",
      "email": "mcerda@mcerda.com",
      "phones": [
        {
          "id": 2,
          "number": "1234567",
          "citycode": "1",
          "contrycode": "57"
        }
      ],
      "created": "2020-08-12T20:07:31.818648",
      "modified": "2020-08-12T20:07:31.818667",
      "lastLogin": "2020-08-12T20:07:31.818664",
      "active": true
    },
    {
      "id": 3,
      "name": "Marco Cerda",
      "password": "$2a$10$d2nv/UxD7y9T32bNKb96w.KgVmc6LK5w/Mm2NHSxsQmQcJfmP6d7.",
      "email": "mcerda@mcerda2.com",
      "phones": [
        {
          "id": 4,
          "number": "1234567",
          "citycode": "1",
          "contrycode": "57"
        }
      ],
      "created": "2020-08-12T20:14:34.480757",
      "modified": "2020-08-12T20:14:34.480765",
      "lastLogin": "2020-08-12T20:14:34.480764",
      "active": true
    },
    {
      "id": 5,
      "name": "Marco Cerda",
      "password": "$2a$10$lMVBeqOUqrkaTdGSGGvKuu/AwVOXzCLpM/bjy4375ucDnGi5vBQZ6",
      "email": "mcerda@mcerda3.com",
      "phones": [
        {
          "id": 6,
          "number": "1234567",
          "citycode": "1",
          "contrycode": "57"
        }
      ],
      "created": "2020-08-12T20:14:38.768267",
      "modified": "2020-08-12T20:14:38.768273",
      "lastLogin": "2020-08-12T20:14:38.768272",
      "active": true
    }
  ]
}
```

### DELETE /user/{userId}
## headers
	- Content-Type: application/json
	- Authorization: Bearer <token>

## request
```json
no body
```

## response
```json
{
  "id": 5,
  "name": "Marco Cerda",
  "password": null,
  "email": "mcerda@mcaedrda2.com",
  "phones": [
    {
      "id": null,
      "number": "1234567",
      "citycode": "1",
      "contrycode": "57"
    }
  ],
  "created": "2020-08-12T20:03:00.924660575",
  "modified": "2020-08-12T20:03:00.924671088",
  "lastLogin": "2020-08-12T20:03:00.924669798",
  "active": true
}
```